require 'httparty'
require 'nokogiri'
require 'csv'

CHARACTERS = ["frasier", "niles", "martin", "roz", "daphne", "bulldog", "lilith", "kenny", "bebe", "gil", "noel"]

def convert_filename(filename)
  # `season_4_episode_1` => `S04E01`
  if match = filename.match(/(\d+)_episode_(\d+)/)
    season, episode = match.captures.map{ |d| d.rjust(2, "0") }
    return "S#{season}E#{episode}"
  end
end

def get_episode_transcript(path)
  puts "opening episode `#{path}`..."
  url      = "http://www.kacl780.net#{path}"
  filename = convert_filename(url.split('/')[5] + "_" + url.split('/')[6])
  return if File.exist? "transcripts/#{filename}.csv"

  page   = HTTParty.get(url)
  parsed = Nokogiri::HTML(page.to_s)
  parsed.search("i", "em", "center").remove

  transcript_el = parsed.search('h2[contains("Transcript")] + pre')
  if transcript_el.empty?
    transcript_el = parsed.search('h2[text()~="Quotes"] + pre')
  end

  write_dialogue(transcript_el, filename)
end

def write_dialogue(transcript_el, filename)
  dialogues         = []
  current_character = nil

  transcript_el[0]&.children&.each do |node|
    line_text = node.text.encode("UTF-8", invalid: :replace, replace: '')
                         .delete("[ ]", "[]")
                         .squeeze(" ")
    next if line_text.empty?

    if node.name == "b"
      current_character = line_text.gsub(":", "").strip
      dialogues << {
        character: current_character,
        line: ""
      }
    else
      if current_character
        dialogues.last[:line] += " #{line_text.strip}"
        dialogues.last[:line].delete! "\r\n"
        dialogues.last[:line].strip!
      end
    end
  end

  puts "saving transcript to `transcripts/#{filename}.csv`..."

  CSV.open("transcripts/#{filename}.csv", "w") do |csv|
    dialogues.each do |d|
      csv << [d[:character], d[:line]]
    end
  end
end

# combines all lines of a character across all episodes into one CSV file
def write_to_collection(episode)
  file = File.read(episode)
  puts episode
  csv  = CSV.parse(file)
  csv.each do |row|
    character, line = row.map(&:downcase)
    CSV.open("character_lines/#{character.gsub("/", "")}.csv", "a") do |char_lines|
      char_lines << [episode, line]
    end
  end
end

# Get the list of episodes, and run get_episode_transcript method for each URL 
index_url    = "http://www.kacl780.net/frasier/transcripts/"
index_page   = HTTParty.get(index_url)
index_parsed = Nokogiri::HTML(index_page.to_s)

index_parsed.css(".SeasonList a").each do |link|
  get_episode_transcript link.attributes["href"].value
end


Dir["transcripts/*.csv"].sort.each do |episode|
  write_to_collection episode
end

# Combines the lines of each character and saves them as one "document" for easier tokenization
CHARACTERS.each do |character|
  document_file = File.open("character_documents/#{character}", "a")

  puts "opening `character_lines/#{character}.csv`..."
  file = File.read "character_lines/#{character}.csv"
  csv  = CSV.parse(file)
  csv.each do |row|
    line = row[1]
    document_file.write "#{line} "
  end
end


