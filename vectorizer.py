import os
import numpy as np
import nltk
import string
import csv
from os import listdir
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse.csr import csr_matrix

characters          = listdir("./character_documents")
filenames_with_path = [os.path.join("./character_documents", fn) for fn in characters]
punctuations        = list(string.punctuation)
is_not_punct        = lambda pos: pos[:2].isalnum()

def tokenize(text):
    # Tokenizes each word in the document and returns only all nouns
    tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    words  = [word for (word, pos) in nltk.pos_tag(tokens) if is_not_punct(pos) & word.isalpha()]
    return words

# gets all character documents and saves their content as corpus
corpus = []
for fn in filenames_with_path:
    with open(fn) as f:
        text = f.read()
        corpus.append(text)

print(characters)

# Initializes and runs the TF-IDF vectorizer. also collects the names of the features generated (the words)
tf = TfidfVectorizer(input = 'content',
                    analyzer = 'word',
                    min_df = 0,
                    stop_words = 'english',
                    tokenizer = tokenize,
                    sublinear_tf = True)
tfidf_matrix  = tf.fit_transform(corpus)
feature_names = tf.get_feature_names()

# Gets each token and score from the TF-IDF matrix for each character document
for index, character in enumerate(characters):
    print("writing top words for " + character + "...\n")

    feature_index = tfidf_matrix[index,:].nonzero()[1]
    tfidf_scores  = zip(feature_index, [tfidf_matrix[index, x] for x in feature_index])
                    # [(index, score), ...]
    tfidf_scores  = reversed(sorted(tfidf_scores, key = lambda t: t[1]))

    with open(os.path.join("top_words", character + ".csv"), "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        for w, s in [(feature_names[i], s) for (i, s) in tfidf_scores]:
            writer.writerow([w, s])
